/* eslint-disable radix */
// import {ProductBundles} from "./modules/ProductBundles";
import './styles/main.scss';
import moment from 'moment/src/moment';
import loadSVGs from './modules/svg-replace';
import 'popper.js';
import 'bootstrap';

document.addEventListener('DOMContentLoaded', () => {
  loadSVGs();
});

// hide 'percent off' container with 0 value for savings amount
const percentOff = document.querySelector(".percent-off[data-base-savings-amount='0']");
percentOff.style.display = 'none';

// fetch modal data
const url = 'https://www.algaecal.com/wp-json/acf/v3/options/options';
// eslint-disable-next-line func-names
fetch(url).then(response => response.json()).then((data) => {
  // eslint-disable-next-line no-use-before-define
  window.onload = modal(data);
  // eslint-disable-next-line no-use-before-define
  window.onload = officeHours(data);
});

// modal function
function modal(data) {
  const content = data.acf['7yr_full_copy'];

  const modalContainer = document.querySelector('#guaranteeModal .modal-body .container');
  const div = document.createElement('div');
  div.innerHTML = JSON.parse(JSON.stringify(content));
  modalContainer.append(div);
}

// office hours function
const today = new Date();
const dayOfWeek = today.getDay() + 1;
// eslint-disable-next-line prefer-template
const time = today.getHours() + ':' + today.getMinutes();
const timeFormatted = moment(time, 'h:mm A').format('HHmm');

function officeHours(data) {
  const hours = data.acf.office_hours;
  hours.forEach((item) => {
    // eslint-disable-next-line max-len
    if (parseInt(dayOfWeek) === parseInt(item.day) && parseInt(timeFormatted) >= parseInt(item.starting_time) && parseInt(timeFormatted) <= parseInt(item.closing_time)) {
      document.querySelector('.speak-to-our-bone-specialists').style.cssText = 'display:block !important';
    } else {
      document.querySelector('.speak-to-our-bone-specialists').style.cssText = 'display:none !important';
    }
  });
}
